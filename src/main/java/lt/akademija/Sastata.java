package lt.akademija;

import java.io.Serializable;
import java.util.Date;

public class Sastata implements Serializable {

	private Integer id;
	private String numeris;
	private Date pagaminimoData;
	private String pagaminusiImone;
	private String miestas;
	private String vagonoTipas;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumeris() {
		return numeris;
	}

	public void setNumeris(String numeris) {
		this.numeris = numeris;
	}

	public Date getPagaminimoData() {
		return pagaminimoData;
	}

	public void setPagaminimoData(Date pagaminimoData) {
		this.pagaminimoData = pagaminimoData;
	}

	public String getPagaminusiImone() {
		return pagaminusiImone;
	}

	public void setPagaminusiImone(String pagaminusiImone) {
		this.pagaminusiImone = pagaminusiImone;
	}

	public String getMiestas() {
		return miestas;
	}

	public void setMiestas(String miestas) {
		this.miestas = miestas;
	}

	public String getVagonoTipas() {
		return vagonoTipas;
	}

	public void setVagonoTipas(String vagonoTipas) {
		this.vagonoTipas = vagonoTipas;
	}
	
	

}
