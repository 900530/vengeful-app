package lt.akademija;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class SastataModel {

	private List<Sastata> vagonai = new ArrayList<Sastata>();
	
	private Sastata redaguojamasVagonas;
	
	public Sastata searchSastataById(Integer id) { 
		for(Sastata list : vagonai){
			if(list.getId() == id){
				return list;
			}
		}
		return redaguojamasVagonas;
		
	}

	public List<Sastata> getVagonai() {
		return vagonai;
	}

	public void setVagonai(List<Sastata> vagonai) {
		this.vagonai = vagonai;
	}

	public Sastata getRedaguojamasVagonas() {
		return redaguojamasVagonas;
	}

	public void setRedaguojamasVagonas(Sastata redaguojamasVagonas) {
		this.redaguojamasVagonas = redaguojamasVagonas;
	}
	
}
